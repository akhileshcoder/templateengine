var errorCodes = {
	100 : "Success message",
	101 : "Invalid username or password", //invalid username
	102 : "Invalid Authentication", //invalid password
	103 : "invalid token",  //invalid website token when generate game token
	104 : "Domain is not registered",
	500 : "Some Server Internal error",
	501 : "Some Server Internal error" //mysql error
};

module.exports = errorCodes;
