const {getTemplate} = require('tempEngineLib')
module.exports = {
  get: [
    {
      url: ["/", "/login"],
      callback: (req, res) => {
        const body = {
          appScriptFile: "login.component.js",
          globalObj: {
          pageId: 'login',
          userData: [{
            team: 'User in login',
            name: 'John Smith in login',
            email: 'john.smith@login.com'
          }],
          queryParam: JSON.stringify(req.query)
        }
        };
        getTemplate({
          "id": "1",
          "name": "angular_package",
          "url": "https://github.com/kirankoirala633/angular-helloworld.git",
          "branch": "master",
          "buildPath": {
            "css": [
              "dist/assets/styles"
            ],
            "js": [
              "dist"
            ]
          },
          "template": "<app-root></app-root>",
          "buildCommand": "npm i && npm run build"
        },body, res)
      }
    },
    {
      url: "/payment",
      callback: (req, res) => {
        const body = {
          appScriptFile: "payment.component.js",
          globalObj: {
          pageId: 'payment',
          userData: [{
            team: 'User in payment',
            name: 'John Smith in payment',
            email: 'john.smith@payment.com'
          }],
          queryParam: JSON.stringify(req.query)
        }
        };
        getTemplate({
          "id": "2",
          "name": "angularjs_package",
          "url": "https://github.com/kirankoirala633/templateengineaanglarfeed.git ",
          "branch": "master",
          "buildPath": {
            "css": [
              "dist/css/todo.css"
            ],
            "js": [
              "dist/js/angular.min.js",
              "dist/js/todolist.js"
            ]
          },
          "template": "<div id=\"bootstrap_id\"><todo-dir></todo-dir></div>",
          "buildCommand": "npm run build"
        },body, res)
      }
    },
    {
      url: "/payment_1",
      callback: (req, res) => {
        const body = {
          appScriptFile: "payment_1.component.js",
          globalObj: {
          pageId: 'payment_1',
          userData: [{
            team: 'User in payment_1',
            name: 'John Smith in payment_1',
            email: 'john.smith@payment_1.com'
          }],
          queryParam: JSON.stringify(req.query)
        }
        };
        getTemplate({
          "id": "3",
          "name": "react_package",
          "url": "https://github.com/kirankoirala633/react-hello-world-app.git",
          "branch": "master",
          "buildPath": {
            "css": [
              "build/static/css"
            ],
            "js": [
              "build/static/js"
            ]
          },
          "template": "<div id=\"root\"></div>",
          "buildCommand": "npm i && npm run build"
        },body, res)
      }
    },
    {
      url: "/ng_with_dist",
      callback: (req, res) => {
        const body = {
          appScriptFile: "payment_1.component.js",
          globalObj: {
          pageId: 'payment_1',
          userData: [{
            team: 'User in payment_1',
            name: 'John Smith in payment_1',
            email: 'john.smith@payment_1.com'
          }],
          queryParam: JSON.stringify(req.query)
        }
        };
        console.log('wrefe');
        getTemplate({
          id: "4",
          name: "angularjs_with_dist",
          url: "https://github.com/kirankoirala633/angularjs_with_dist.git",
          branch: "master",
          buildPath: {
            css: ["todo-list/css/todo.css"],
            js: ["js/angular.min.js", "todo-list/js/todolist.js"]
          },
          template: '<div id="bootstrap_id"><todo-dir></todo-dir></div>',
        },body, res)
      }
    }
  ],
  post: [],
  put: []
};
