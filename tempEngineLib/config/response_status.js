module.exports = {
    INVALID_DATA: "Invalid data schema passed",
    INVALID_TEMPLATE_ID: "Invalid template id passed",
    INVALID_TEMPLATE: "Invalid template passed",
}
