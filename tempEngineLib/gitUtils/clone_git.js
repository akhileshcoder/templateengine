const child_process = require("child_process"),
  path = require("path"),
  fs = require("fs");

module.exports = {
  cloneNBuildGit: (
    {
      name,
      url,
      branch = "master",
      buildPath,
      selector = "",
      buildCommand
    },
    cb
  ) => {
    const destPath = path.resolve(__dirname, "./gitPackages");
    const destProjPath = destPath + "/" + name;
    const logSTD = (typ, error, stdout, stderr) => {
      console.log(`error ${typ}: `, error);
      console.log(`stdout ${typ}: `, stdout);
      console.log(`stderr ${typ}: `, stderr);
    };
    const createBranch = callback => {
      child_process.exec(
        `cd ${destProjPath} && git checkout -b ${branch} --track origin/${branch}`,
        { cwd: __dirname, maxBuffer: 1024 * 1024 },
        (error, stdout, stderr) => {
          logSTD("checkout", error, stdout, stderr);
          callback();
        }
      );
    };
    const getBranch = callback => {
      child_process.exec(
        `cd ${destProjPath} && git branch`,
        { cwd: __dirname, maxBuffer: 1024 * 1024 },
        (error, stdout, stderr) => {
          logSTD("branch", error, stdout, stderr);
          if (!error && !stderr) {
            let branchName = stdout.split(" ");
            callback(branchName[branchName.length - 1].trim());
          }
        }
      );
    };
    const pullBranch = callback => {
      child_process.exec(
        `cd ${destProjPath} && git pull origin ${branch}`,
        { cwd: __dirname, maxBuffer: 1024 * 1024 },
        (error, stdout, stderr) => {
          logSTD("checkout", error, stdout, stderr);
          callback();
        }
      );
    };

    const checkDistFolder = callback => {
      let distAssetPath = buildPath.js.length
        ? buildPath.js[0]
        : buildPath.css.length
        ? buildPath.css[0]
        : false;

      if (distAssetPath && buildCommand) {
        const distPath = destProjPath + "/" + distAssetPath;
        if (fs.existsSync(distPath)) {
          fs.readdir(distPath, function(err, files) {
            callback(!files.length);
          });
        } else {
          callback(true);
        }
      } else {
        callback(false);
      }
    };

    const installNBuildPackage = () => {
      checkDistFolder(noDistAvailable => {
        if (noDistAvailable) {
          child_process.exec(
            `cd ${destProjPath} && ${buildCommand}`,
            { cwd: destProjPath, maxBuffer: 1024 * 1024 },
            (error, stdout, stderr) => {
              logSTD("install and build", error, stdout, stderr);
              cb();
            }
          );
        } else {
          cb();
        }
      });
    };

    const getCreatePullBuildBranch = () => {
      getBranch(branchName => {
        if (branchName !== branch) {
          createBranch(() => {
            pullBranch(installNBuildPackage);
          });
        } else {
          pullBranch(installNBuildPackage);
        }
      });
    };
    const sshKeyPath = global.templateEngineConfig.sshKeyPath;
    if (fs.existsSync(destProjPath)) {
      getCreatePullBuildBranch();
    } else {
      child_process.exec(
        `ssh-agent bash -c 'ssh-add ${sshKeyPath}; git clone ${url} ./gitPackages/${name}'`,
        { cwd: __dirname, maxBuffer: 1024 * 1024 },
        (error, stdout, stderr) => {
          logSTD("clone", error, stdout, stderr);
          getCreatePullBuildBranch();
        }
      );
    }
  }
};
