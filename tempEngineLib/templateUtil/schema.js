module.exports = {
  repoSchema: {
    type: "object",
    properties: {
      id: { type: "string" },
      name: { type: "string" },
      url: { type: "string" },
      branch: { type: "string" },
      buildPath: {
        css: {
          type: "array",
          items: { type: "string" }
        },
        js: {
          type: "array",
          items: { type: "string" }
        }
      },
      selector: { type: "string" },
      buildCommand: { type: "string" }
    },
    required: ["id", "name", "url", "buildPath"]
  },
  dataSchema: {
    type: "object",
    properties: {
      id: { type: "string" },
      data: { type: "object" }
    },
    required: ["id"]
  }
};
