const {
  INVALID_DATA,
  INVALID_TEMPLATE,
  INVALID_TEMPLATE_ID
} = require("../config/response_status");
const { validateData, validateRepo } = require("./validate_data");
const { cloneNBuildGit } = require("../gitUtils/clone_git");
const fs = require("fs");
const path = require("path");
const getFileList = (name, dir) => {
  //server/node_modules/tempenginelib/gitUtils/gitPackages/angularjs_package
  const dirPath = path.resolve(
    __dirname,
    `../gitUtils/gitPackages/${name}/${dir}`
  );
  console.log("dirPath: ", dirPath);
  return fs.readdirSync(dirPath);
};
const updateCSSNJS = template => {
  //server/node_modules/tempenginelib/gitUtils/gitPackages/angularjs_package
  let clonedTemp = JSON.parse(JSON.stringify(template));
  ["css", "js"].forEach(fileType => {
    const flieExt = "." + fileType;
    clonedTemp.buildPath[fileType] = clonedTemp.buildPath[fileType].reduce(
      (a, b) => {
        a = a.concat(
          b.endsWith(flieExt)
            ? [b]
            : getFileList(template.name, b).map(
                i => `${template.name}/${b}/${i}`
              )
        );
        return a;
      },
      []
    );
    clonedTemp.buildPath[fileType] = clonedTemp.buildPath[fileType]
      .filter(i => i.endsWith(flieExt))
      .map(i => (i.startsWith(template.name) ? i : template.name + "/" + i));
  });
  return clonedTemp;
};
module.exports = {
  getTemplate: (templateObj, data, res) => {
    const repos =
      global.templateEngineConfig && global.templateEngineConfig.repos
        ? global.templateEngineConfig.repos
        : [];
    const staticPath =
      global.templateEngineConfig && global.templateEngineConfig.staticPath
        ? global.templateEngineConfig.repos
        : [];
    const repoObj = repos.find(
      i => i.id + "" === (templateObj && templateObj.id + "")
    );
    if (!repoObj) {
      if (!validateRepo(templateObj)) {
        res.render(INVALID_TEMPLATE);
      } else {
        const jsnPth = path.resolve(
          __dirname,
          "../gitUtils/_init/git_repos.json"
        );
        console.log("jsnPth:;", jsnPth);
        repos.push(templateObj);
        global.templateEngineConfig = {
          ...(global.templateEngineConfig || {}),
          repos
        };
        cloneNBuildGit(templateObj, () => {
          res.render("home", {
            template: updateCSSNJS(templateObj),
            data: JSON.stringify(data)
          });
          fs.writeFileSync(jsnPth, JSON.stringify(repos, null, 4), "utf8");
        });
      }
    } else if (!validateData(data)) {
      res.render(INVALID_DATA);
    } else {
      res.render("home", {
        template: updateCSSNJS(repoObj),
        data: JSON.stringify(data)
      });
    }
  }
};
