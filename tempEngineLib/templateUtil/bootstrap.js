const path = require("path"),
  exphbs = require("express-handlebars"),
  { initialiseRepos } = require("../gitUtils/_init/initialise");

module.exports = {
  bootstrap: function(app, express, staticPath, sshKeyPath='~/.ssh/id_rsa', cb) {
    const handelBarTemplatePath = path.resolve(__dirname, "../handelBarTemplate");
    staticPath = staticPath ? staticPath : path.resolve(__dirname, "../gitUtils/gitPackages");
    global.templateEngineConfig = {
      ...(global.templateEngineConfig || {}),
      staticPath, sshKeyPath
    };
    app.use(express.static(staticPath));
    app.engine(
      "handlebars",
      exphbs({
        defaultLayout: "main",
        layoutsDir: handelBarTemplatePath+"/layouts"
      })
    );
    app.set("view engine", "handlebars");
    app.set("views", handelBarTemplatePath);
    initialiseRepos(cb);
  }
};
